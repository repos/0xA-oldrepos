/* ctlsig~ ::  [phasor~] without fmod */

#include "m_pd.h"

static t_class *phasor_class, *scalarphasor_class;

typedef struct _phasor {
  t_object x_obj;
  double x_phase;
  float x_conv;
  float x_f;      /* scalar frequency */
  t_outlet *outlet;
} t_phasor;

static void *phasor_new(t_floatarg f) {
  t_phasor *x = (t_phasor *)pd_new(phasor_class);
  x->x_f = f;
  inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_float, gensym("ft1"));
  //inlet_new(&x->x_obj, &x->x_obj.ob_pd, &s_signal, &s_signal);
  x->x_phase = 0;
  x->x_conv = 0;
  outlet_new(&x->x_obj, gensym("signal"));
  return (x);
}

static t_int *phasor_perform(t_int *w) {
  t_phasor *x = (t_phasor *)(w[1]);
  t_float *in = (t_float *)(w[2]);
  //t_float *in2 = (t_float *)(w[3]);
  t_float *out = (t_float *)(w[3]);
  int n = (int)(w[4]);
  double dphase = x->x_phase;
  float conv = x->x_conv;

  while (n--) {
    dphase += *in++ * conv;
/*     if (*in2 <= 0) { */
/*       dphase = 0; */
/*     } else { */
/*       dphase += *in++ * conv; */
/*     } */
    *out++ = dphase;
  }
  x->x_phase = dphase;
  return (w+5);
}

static void phasor_dsp(t_phasor *x, t_signal **sp) {
  x->x_conv = 1./sp[0]->s_sr;
  dsp_add(phasor_perform, 4, x, sp[0]->s_vec, sp[1]->s_vec, sp[0]->s_n);
  //dsp_add(phasor_perform, 5, x, sp[0]->s_vec, sp[1]->s_vec, sp[2]->s_vec, sp[0]->s_n);
}

static void phasor_ft1(t_phasor *x, t_float f) {
  x->x_phase = f;
}

void ctlsig_tilde_setup(void) {
  phasor_class = class_new(gensym("ctlsig~"), (t_newmethod)phasor_new, 0,
			   sizeof(t_phasor), 0, A_DEFFLOAT, 0);
  CLASS_MAINSIGNALIN(phasor_class, t_phasor, x_f);
  class_addmethod(phasor_class, (t_method)phasor_dsp, gensym("dsp"), 0);
  class_addmethod(phasor_class, (t_method)phasor_ft1,
		  gensym("ft1"), A_FLOAT, 0);
}

