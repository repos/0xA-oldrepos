(
var song, song_f, l00p, l00p_f;
var clock_f, two_f, one_f, three_f, four_f, five_f;
var clock, two, one, three, four, five;
var one_val, two_val, three_val, four_val, five_val;
var two_v, one_v, three_v, four_v, five_v;
var eq, rvt, ein, eout;
var b_phs16_s, m_count, m_mul;
//utilities
eq = {arg in, con, t,f; if(in>(con-1), if(in<(con+1), t, f), f)};
rvt = {arg in; abs(in-1)};
ein = {arg in, curve=10; ((-1*(pow(2,(-1*curve)*in)))+1)};
eout = {arg in, curve=10; pow(2,(curve*10)*(in-1))};
//control buses for the valumes 
one_val = Bus.control(s,3); two_val = Bus.control(s,2);
three_val = Bus.control(s,4); four_val = Bus.control(s,5);
five_val = Bus.control(s,6);
two_val.set(0); one_val.set(0);
three_val.set(0); four_val.set(0); five_val.set(0);
//control signal
clock_f = {
	var freq, phs16_s, bpm, t_bars;
	bpm = 168;
	t_bars = 256;
	freq = 1/((60*(t_bars*4))/bpm);
	b_phs16_s = Bus.control(s,1);
	phs16_s = LFSaw.kr(freq,-1.001).range(0,1);
	Poll.ar(K2A.ar(((((phs16_s*256)%1.0)-0.5)%1.0)-0.5),
		floor(phs16_s*256), \bar);
	Out.kr(b_phs16_s, phs16_s);
};
//first section 
one_f = {
	var phs16_s, phs2_s, phs_s, bar_s, qtr_s, eth_s, stn_s;
	var bar_c, qtr_c, eth_c, stn_c;

	var b, base, freq, env;
	var raw, local;
	var kik_freq, kik_raw, kik_out, kik_env;
	var hat_env, hat_raw, hat_out, hat_seq, hat_seq2;
	
	phs16_s = In.kr(b_phs16_s);
	phs2_s = (phs16_s*16)%1.0;
 	phs_s = (phs2_s*2)%1.0;
 	bar_s = (phs2_s*8)%1.0;
 	qtr_s = (bar_s*4)%1.0;
	eth_s = (qtr_s*2)%1.0;
 	stn_s = (qtr_s*4)%1.0;
	bar_c = floor(phs2_s*8);
	qtr_c = floor(phs_s*16);
	eth_c = floor(phs_s*32);
	stn_c = floor(phs_s*64);
	//Bass
	b = 24;
	base = eq.value(bar_c%2,1,
		eq.value(bar_c%4,3, eq.value(floor(phs2_s*2),1, b+7,b+3), b+5),b);
	freq = Slew.ar(
		K2A.ar(eq.value(
			(stn_c)%4, 2,
			eq.value(bar_c, 3, base.midicps*4, base.midicps*2),
			base.midicps)),
		1000, 1000);
	env = pow(
		eq.value(stn_c%16, 7, (rvt.value(stn_s)*2)%1.0, rvt.value(stn_s)), 3);
	raw = MoogVCF.ar(Latch.ar(SinOsc.ar(freq, 0),Impulse.ar(freq*3)).dup*env, (bar_s*1000)+100, 0.8);
	local = LocalIn.ar(2)+[	MoogVCF.ar(raw, (bar_s*700)+100, 0.8),0];
	local = DelayN.ar(local, 1,0.25);
	LocalOut.ar(local.reverse * 0.1 * In.kr(one_val));
	Out.ar(0, (local+(raw*0.8))*0.5*In.kr(one_val));
	//Kick
	kik_env = eq.value(eth_c%8, 7, stn_s, eth_s);
	kik_freq = 100*pow(rvt.value(kik_env), 2)+20;
	kik_raw = SinOsc.ar(kik_freq);
	kik_out = kik_raw*pow(rvt.value(kik_env),2)*0.5;
	kik_out = kik_out*(floor(phs2_s*2));
	Out.ar(0, Pan2.ar(kik_out*In.kr(one_val),-0.3, 0.5));
	//Hi-hat
	hat_seq = eq.value(floor(phs_s*8),7,(stn_s*2)%1.0,eth_s);
	hat_seq2 = eq.value(floor(phs_s*8),7,0,0.5);
	hat_env = pow(((rvt.value(hat_seq)+eq.value(eth_c%4, 3, 0, hat_seq2))%1.0),
		8)*0.15;
 	hat_raw = MoogVCF.ar(WhiteNoise.ar()*hat_env, qtr_s*5000, 0.8);
	hat_raw = hat_raw*(eq.value(floor(phs2_s*4), 3, 1, 0));
 	Out.ar(0, Pan2.ar(hat_raw*4*In.kr(one_val)*0.5,
	Latch.ar(WhiteNoise.ar(),((stn_s-0.5)%1.0)-0.5)));
};
//second section
two_f = {
	var phs16_s;
	var phs2_s, phs_s, bar_s, qtr_s, eth_s, stn_s;
	var bar_c, qtr_c, eth_c, stn_c;

	var b, base, freq, env;
	var raw, local;

	var m_freq, m_env, m_raw, m_out, m_base, m_mod, m_del;
	var m_freq_rvt,m_out_rvt;
	var ap_freq, ap_env, ap_raw, ap_out, ap_base;
	var kik_freq, kik_raw, kik_out, kik_env;
	var hat_env, hat_raw, hat_out;

	phs16_s = In.kr(b_phs16_s);
	phs2_s = (phs16_s*16)%1.0;
 	phs_s = (phs2_s*2)%1.0;
 	bar_s = (phs2_s*8)%1.0;
 	qtr_s = (bar_s*4)%1.0;
	eth_s = (qtr_s*2)%1.0;
 	stn_s = (qtr_s*4)%1.0;
	bar_c = floor(phs2_s*8);
	qtr_c = floor(phs_s*16);
	eth_c = floor(phs_s*32);
	stn_c = floor(phs_s*64);
	//bass
	b = 24;
	base = eq.value(bar_c%2,1, eq.value(bar_c%4,3,
		eq.value(floor(phs2_s*2),1, b+7,b+3), b+5),b);
	freq = Slew.ar(
		K2A.ar(eq.value(
			(stn_c)%4, 2,
			eq.value(bar_c, 3, base.midicps*4, base.midicps*2),
			base.midicps)),
		1000, 1000);
	env = pow(
		eq.value(stn_c%16, 7, (rvt.value(stn_s)*2)%1.0, rvt.value(stn_s)), 3);
	
	raw = MoogVCF.ar(Latch.ar(SinOsc.ar(freq, 0),Impulse.ar(freq*3)).dup*env, (bar_s*1000)+100, 0.8);
	local = LocalIn.ar(2)+[	MoogVCF.ar(raw, (bar_s*700)+100, 0.8),0];
	local = DelayN.ar(local, 1,0.25);
	LocalOut.ar(local.reverse * 0.1 * In.kr(two_val));
	Out.ar(0, ((local+(raw*1))*0.5)*In.kr(two_val));
	//arpeggio
	ap_base = eq.value(bar_c%2,1,eq.value(bar_c,3,
		eq.value(floor(phs2_s*2), 1, b+4, b+5),b+7),b);
	ap_freq = ap_base.midicps*(((floor(eth_s*eq.value(eth_c%8,7, 6,16))%2)+1)*12);
	ap_env = pow(rvt.value(eq.value(eth_c%8, 7, eth_s, stn_s)), 2);
	ap_env = ap_env*floor(qtr_s*2)*eq.value(eth_c%8, 7, 1, floor(eth_s*2));
	ap_raw = Latch.ar(SinOsc.ar(ap_freq),
		Impulse.ar(ap_freq*((floor(eth_s*2)+1)*4)))*ap_env;
		ap_out = MoogVCF.ar(ap_raw, (rvt.value(pow(stn_s,2))*5000)+1000,0.85);
	Out.ar(0, Pan2.ar(ap_out,
		eq.value(floor(eth_s*16)%2,0,-0.6,0.6),
		0.3)*In.kr(two_val)*0.6);
	//kick
	kik_env = eq.value(eth_c%8, 7, stn_s, eth_s);
	kik_freq = 100*pow(rvt.value(kik_env),4)+20;
	kik_raw = SinOsc.ar(kik_freq);
	kik_out = kik_raw*pow(rvt.value(kik_env),2)*0.5;
	Out.ar(0, Pan2.ar(kik_out*In.kr(two_val),-0.3,0.5));
	//hi-hit
	hat_env = pow(((rvt.value(eth_s)+eq.value(eth_c%4, 3, 0, 0.5))%1.0),8)*0.15;
	hat_raw = MoogVCF.ar(WhiteNoise.ar()*hat_env, qtr_s*5000, 0.8)*0.5;
	Out.ar(0, Pan2.ar(hat_raw*4*In.kr(two_val),-0.3));
	//melody
	m_mod = eq.value(floor(phs_s*4),3,4,3);
	m_freq = Slew.ar(K2A.ar(((floor(bar_s*8)%m_mod)+1)*60.midicps),
		5000,1000)+((((stn_s*4)%1.0)*100)*(eout.value((bar_s*2)%1.0,2)));
	m_env = eq.value(floor(bar_s*2), 0, 1, 0)*ein.value(bar_s,10);
	m_raw = Latch.ar(
		SinOsc.ar(m_freq,0,0.2)*m_env*(eq.value(floor(phs2_s*2),1,floor(stn_s*8)%2,1)), 
		Impulse.ar(m_freq*8));
	m_out = Pan2.ar(m_raw,-0.3,0.7);

	m_del = LocalIn.ar(2)+[m_out,0];
	m_del = DelayN.ar(m_del, 1,0.125);
	LocalOut.ar(m_del.reverse * 0.2 * In.kr(two_val));
	Out.ar(0, ((m_out*0.3)+(m_del*0.1))*In.kr(two_val));
};
//Thrid section
three_f = {
	var phs16_s;
	var phs2_s, phs_s, bar_s, qtr_s, eth_s, stn_s;
	var phs_c, bar_c, qtr_c, eth_c, stn_c;

	var m_freq, m_env, m_raw, m_out, m_base, m_mod, m_del;
	var m_freq2, m_env2, m_raw2, m_out2, m_base2, m_mod2, m_del2, m_off2, m_tran;
	var m_freq3, m_env3, m_raw3, m_out3, m_base3, m_mod3, m_del3;
	var kik_freq, kik_raw, kik_out, kik_env;
	var hat_env, hat_raw, hat_out;

	phs16_s = In.kr(b_phs16_s);
	phs2_s = (phs16_s*16)%1.0;
 	phs_s = (phs2_s*2)%1.0;
 	bar_s = (phs2_s*8)%1.0;
 	qtr_s = (bar_s*4)%1.0;
	eth_s = (qtr_s*2)%1.0;
 	stn_s = (qtr_s*4)%1.0;
	phs_c = floor(phs2_s*2);
	bar_c = floor(phs2_s*8);
	qtr_c = floor(phs_s*16);
	eth_c = floor(phs_s*32);
	stn_c = floor(phs_s*64);
	//hi-hat
	hat_env = pow(((rvt.value(eth_s)+eq.value(eth_c%4, 3, 0, 0.5))%1.0),8)*0.15;
	hat_raw = MoogVCF.ar(WhiteNoise.ar()*hat_env, qtr_s*5000, 0.8);
	hat_out = Pan2.ar(hat_raw,
	Latch.ar(WhiteNoise.ar(),((eth_s-0.5)%1.0)-0.5),1);
	Out.ar(0, hat_out*4*In.kr(three_val).dup);
	//melody
	m_mod = eq.value(bar_c%4,3,4,3);
	m_freq = Slew.ar(K2A.ar((((eth_c%8)%m_mod)+1)*60.midicps),
		5000,1000)+((((stn_s*4)%1.0)*100)*(eout.value((bar_s*2)%1.0,2)));
	m_env = eq.value(floor(bar_s*2), 0, 1, 0)*ein.value(bar_s,10);
	m_raw = Latch.ar(
		SinOsc.ar(m_freq,0,0.2)*m_env*(eq.value((phs_c%2),1,floor(stn_s*8)%2,1)), 
		Impulse.ar(m_freq*8));
	m_raw = MoogVCF.ar(m_raw, (bar_s*10000)+50, 0.3);
	m_out = Pan2.ar(m_raw,-0.3,1.5);
	//melody2
	m_off2 = 0.5;
	m_tran = eq.value((phs_c%2),1,7,1);
	m_mod2 = eq.value((bar_c%4),3,4,3);
	m_freq2 = K2A.ar(((floor(((bar_s-m_off2)%1.0)*8)%m_mod2)+m_tran)*36.midicps);
	m_freq2 = Slew.ar(m_freq2, 500,1000);
	m_env2 = eq.value(floor(((bar_s-m_off2)%1.0)*2), 0,
		1,
		0)*ein.value(((bar_s-m_off2)%1.0),10);
	m_raw2 = Latch.ar(
		SinOsc.ar(m_freq2,0,0.2)*m_env2, 
		Impulse.ar(m_freq2*4));
	m_raw2 = MoogVCF.ar(m_raw2, ((bar_s-m_off2)%1.0)*5000, 0.8);
	m_out2 = Pan2.ar(m_raw2,0.3,1);
	//bass
	m_mod3 = eq.value(eth_c%2,1,4,2);
	m_freq3 = Slew.ar(K2A.ar(((eth_c%m_mod3)+1)*24.midicps),
		5000,1000)+((((stn_s*4)%1.0)*100)*(eout.value((bar_s*2)%1.0,2)));
	m_env3 = pow((rvt.value(bar_s)*16)%1.0,3);
	m_raw3 = Latch.ar(
		SinOsc.ar(m_freq3,0,0.2)*m_env3, 
		Impulse.ar(m_freq3*3));
	m_raw3 = MoogVCF.ar(m_raw3,(rvt.value(qtr_s)*5000)+100, 0.6);
	m_out3 = Pan2.ar(m_raw3,0,2);
	Out.ar(0, (m_out3)*In.kr(three_val));
	//----------------	
	m_del = LocalIn.ar(2)+[	m_out+m_out2,0];
	m_del = DelayN.ar(m_del, 1,0.5);
	LocalOut.ar(m_del.reverse * 0.325 * In.kr(three_val));
	Out.ar(0, (((m_out+m_out2)*0.5)+(m_del*0.15))*In.kr(three_val));
};
//Forth section
four_f = {
	var phs16_s;
	var phs2_s, phs_s, bar_s, qtr_s, eth_s, stn_s;
	var phs_c, bar_c, qtr_c, eth_c, stn_c;

	var b, base, freq, env;
	var m_freq, m_env, m_raw, m_out, m_base, m_mod, m_del, m_cutf;
	var m_freq2, m_env2, m_raw2, m_out2, m_base2, m_mod2, m_del2, m_off2, m_tran;
	var m_freq3, m_env3, m_raw3, m_out3, m_base3, m_mod3, m_del3;
	var kik_freq, kik_raw, kik_out, kik_env;
	var hat_env, hat_raw, hat_out, hat_sig;

	phs16_s = In.kr(b_phs16_s);
	phs2_s = (phs16_s*16)%1.0;
 	phs_s = (phs2_s*2)%1.0;
 	bar_s = (phs2_s*8)%1.0;
 	qtr_s = (bar_s*4)%1.0;
	eth_s = (qtr_s*2)%1.0;
 	stn_s = (qtr_s*4)%1.0;
	phs_c = floor(phs2_s*2);
	bar_c = floor(phs2_s*8);
	qtr_c = floor(phs_s*16);
	eth_c = floor(phs_s*32);
	stn_c = floor(phs_s*64);
	//hi-hat
	hat_sig = eq.value(phs_c%2,0,eth_s, stn_s);
	hat_env = pow(((rvt.value(hat_sig)+eq.value(eth_c%4, 3, 0, 0.5))%1.0),8)*0.15;
	hat_raw = MoogVCF.ar(WhiteNoise.ar()*hat_env, qtr_s*3000, 0.8);
	hat_out = Pan2.ar(hat_raw,
	Latch.ar(WhiteNoise.ar(),((eth_s-0.5)%1.0)-0.5),2);
	Out.ar(0, hat_out*4*In.kr(four_val).dup*0.5);
	//melody
	m_freq = ((((qtr_c-1)%2)*5)-((stn_c%2)*1))*((qtr_c-1)%2);
	m_freq = eq.value(qtr_c%4,2,0,eq.value(qtr_c%4,1,12,0))+m_freq;
	m_freq = Slew.ar(K2A.ar((m_freq+60).midicps), 500, 5000);

	m_env = eq.value(floor(qtr_s*2), 0, eq.value(qtr_c%4,3, eth_s, qtr_s), stn_s);
	m_env = eq.value(floor(bar_s*4),
		eq.value(bar_c%2,1,1,2),
		pow(rvt.value(((floor(eth_s*3)+1)*stn_s)%1.0),2),
		pow(rvt.value(m_env),2));
	m_raw = Latch.ar(
		SinOsc.ar(m_freq,0,0.2)*m_env, 
		Impulse.ar(m_freq*4));
	m_cutf = eq.value(qtr_c%4, 2, stn_s, qtr_s);
	m_raw = MoogVCF.ar(m_raw,
		(ein.value(m_cutf,5)*((m_freq*5))+(m_freq/3)), 0.5);
	m_out = Pan2.ar(m_raw,0.2,1);
	//melody2
	m_mod2 = eq.value(bar_c%4,3,4,3);
	m_freq2 = ((((eth_c%8)%m_mod2)+1)*72.midicps)+((((stn_s*4)%1.0)*100)*(eout.value((bar_s*2)%1.0,2)));
	m_env2 = eq.value(floor(bar_s*2), 0, 1, 0);
	m_raw2 = Latch.ar(
		SinOsc.ar(m_freq2,0,0.2)*m_env2*eq.value(floor(stn_s*3),0,1,0), 
		Impulse.ar(m_freq2*8));
	m_raw2 = MoogVCF.ar(m_raw2, (bar_s*10000)+50, 0.3);
	m_out2 = Pan2.ar(m_raw2,eq.value(stn_c%2,0,-0.5,0.5),1.5);

	m_del = LocalIn.ar(2)+[(m_out+m_out2),0];
	m_del = DelayN.ar(m_del, 1,0.25);
	LocalOut.ar(m_del.reverse * 0.325 * In.kr(four_val));
	Out.ar(0, (((m_out+m_out2)*0.5)+(m_del*0.2))*In.kr(four_val));
	//bass
	b = 24;
 	base = eq.value(bar_c%2,1,
		eq.value(bar_c%4,3,
			eq.value(floor(phs2_s*2),1, b+7,b+3),
			b+5),b);
	m_mod3 = eq.value(eth_c%2,1,4,2);
	m_mod3 = eq.value(qtr_c%4,3,
		eq.value(stn_c%2,0,4,1),
		eq.value((stn_c)%3,0,4,1)); 
	m_freq3 = m_mod3*base.midicps;
	m_env3 = pow((rvt.value(bar_s)*16)%1.0,2);
	m_raw3 = Latch.ar(
		SinOsc.ar(m_freq3,0,0.2)*m_env3, 
		Impulse.ar(m_freq3*3));
	m_raw3 = MoogVCF.ar(m_raw3,(pow(rvt.value(eth_s),2)*3000)+50, 0.8);
	m_out3 = Pan2.ar(m_raw3,0,2);
	Out.ar(0, (m_out3)*In.kr(four_val));
	//kick
	kik_env = eq.value(eth_c%8, 0, (stn_s*2)%1.0,
		eq.value(eth_c%8,4,stn_s,eth_s));
	kik_freq = 100*pow(rvt.value(kik_env),2)+20;
	kik_raw = SinOsc.ar(kik_freq);
	kik_out = kik_raw*pow(rvt.value(kik_env),2)*0.5;
	Out.ar(0, Pan2.ar(kik_out*In.kr(four_val),-0.3,0.5));

};
//Fifth section
five_f = {
	var phs16_s, phs2_s, phs_s, bar_s, qtr_s, eth_s, stn_s;
	var bar_c, qtr_c, eth_c, stn_c;

	var b, base, freq, env;
	var raw, local;
	var m_del;
	var m_freq2, m_env2, m_raw2, m_out2, m_base2, m_mod2, m_del2, m_off2, m_tran;
	var kik_freq, kik_raw, kik_out, kik_env;
	var hat_env, hat_raw, hat_out, hat_seq, hat_seq2;
	
	phs16_s = In.kr(b_phs16_s);
	phs2_s = (phs16_s*16)%1.0;
 	phs_s = (phs2_s*2)%1.0;
 	bar_s = (phs2_s*8)%1.0;
 	qtr_s = (bar_s*4)%1.0;
	eth_s = (qtr_s*2)%1.0;
 	stn_s = (qtr_s*4)%1.0;
	bar_c = floor(phs2_s*8);
	qtr_c = floor(phs_s*16);
	eth_c = floor(phs_s*32);
	stn_c = floor(phs_s*64);
	//bass
	b = 24;
	base = eq.value(bar_c%2,1,
		eq.value(bar_c%4,3, eq.value(floor(phs2_s*2),1, b+7,b+3), b+5),b);
	freq = Slew.ar(
		K2A.ar(eq.value(
			(stn_c)%4, 2,
			eq.value(bar_c, 3, base.midicps*4, base.midicps*2),
			base.midicps)),
		1000, 1000);
	env = pow(
		eq.value(stn_c%16, 7, (rvt.value(stn_s)*2)%1.0, rvt.value(stn_s)), 3);
	raw = MoogVCF.ar(Latch.ar(SinOsc.ar(freq, 0),Impulse.ar(freq*3)).dup*env, (bar_s*1000)+100, 0.8);
	local = LocalIn.ar(2)+[	MoogVCF.ar(raw, (bar_s*700)+100, 0.8),0];
	local = DelayN.ar(local, 1,0.25);
	LocalOut.ar(local.reverse * 0.1 * In.kr(five_val));
	Out.ar(0, (local+(raw*0.8))*0.5*In.kr(five_val));
	//melody
	m_mod2 = eq.value(bar_c%4,3,4,3);
	m_freq2 = ((((eth_c%8)%m_mod2)+1)*72.midicps)+((((stn_s*4)%1.0)*100)*(eout.value((bar_s*2)%1.0,2)));
	m_env2 = eq.value(floor(bar_s*2), 0, 1, 0);
	m_raw2 = Latch.ar(
		SinOsc.ar(m_freq2,0,0.2)*m_env2*eq.value(floor(stn_s*3),0,
			ein.value(stn_s),0), 
		Impulse.ar(m_freq2*8));
	m_raw2 = MoogVCF.ar(m_raw2, (bar_s*10000)+50, 0.3);
	m_out2 = Pan2.ar(m_raw2,eq.value(stn_c%2,0,-0.5,0.5),1);
	m_del = InFeedback.ar(2) + m_out2;
	m_del = DelayN.ar(m_del, 1, 0.125);
	Out.ar(2, m_del.reverse*0.5);
	Out.ar(0, ((m_out2+m_del)*1)*In.kr(five_val));
	//kick
	kik_env = eq.value(eth_c%8, 7, stn_s, eth_s);
	kik_freq = 100*pow(rvt.value(kik_env), 2)+20;
	kik_raw = SinOsc.ar(kik_freq);
	kik_out = kik_raw*pow(rvt.value(kik_env),2)*0.5;
	kik_out = kik_out*(abs(floor(phs2_s*2)-1));
	Out.ar(0, Pan2.ar(kik_out*In.kr(five_val),-0.3, 0.5));
	//hi-hat
	hat_seq = eq.value(floor(phs_s*8),7,(stn_s*2)%1.0,eth_s);
	hat_seq2 = eq.value(floor(phs_s*8),7,0,0.5);
	hat_env = pow(((rvt.value(hat_seq)+eq.value(eth_c%4, 3, 0, hat_seq2))%1.0),
		8)*0.15;
 	hat_raw = MoogVCF.ar(WhiteNoise.ar()*hat_env, qtr_s*5000, 0.8);
	hat_raw = hat_raw*(abs(floor(phs2_s*2)-1));
 	Out.ar(0, Pan2.ar(hat_raw*4*In.kr(five_val)*0.5,
	Latch.ar(WhiteNoise.ar(),((stn_s-0.5)%1.0)-0.5)));
};
//Song structure
song_f = {
	// A B C B | D B C A
 	m_count = floor(In.kr(b_phs16_s)*16)%8;
	one_v = eq.value(m_count,0, 1,0);
	two_v = eq.value(m_count,1,1,
		eq.value(m_count,3,1,
		eq.value(m_count,5,1,0)));
	three_v = eq.value(m_count,2, 1,
		eq.value(m_count,6,1,0));
	four_v = eq.value(m_count,4,1,0);
	five_v = eq.value(m_count,7,1,0);
	//m_mul = 1;
	m_mul = Lag.ar(K2A.ar(abs(floor(In.kr(b_phs16_s)*2)-1)),10);
	Out.kr(one_val,one_v*m_mul);	
	Out.kr(two_val,two_v*m_mul);
	Out.kr(three_val,three_v*m_mul);
	Out.kr(four_val,four_v*m_mul);
	Out.kr(five_val,five_v*m_mul);
};
//Looping 
l00p_f = {
 	four_v = 1;
	Out.kr(one_val,one_v);	
	Out.kr(two_val,two_v);
	Out.kr(three_val,three_v);
	Out.kr(four_val,four_v);
	Out.kr(five_val,five_v);
};

clock = clock_f.play;
two = two_f.play;
one = one_f.play;
three = three_f.play;
four = four_f.play;
five = five_f.play;

song = song_f.play;
//l00p = l00p_f.play
)

s.boot;
s.quit;
s.queryAllNodes;
