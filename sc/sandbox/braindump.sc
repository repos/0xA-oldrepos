// learning from Karsten ;)


Server.default = Server.local;
Server.local;
s.boot;

(
{
	arg level=0.25; 
	//var source = VarSaw.ar([30,32],0,LFNoise2.kr([0.3,0.2]).range(0,1));
	var source = Pulse.ar([32,30],LFNoise2.kr([0.25,0.75]).range(0,1));
	var mousex = MouseX.kr(5, 10, 1);
	var mousey = MouseY.kr(2, 100, 1);
	source = [ 
		XFade2.ar( 
			RHPF.ar(source[0],LFNoise2.ar(2.5).range(500,7000),0.4), 
			RLPF.ar(source[0],LFNoise2.ar(2.5).range(50,2000),0.25), SinOsc.ar(mousex), level),
		XFade2.ar( 
			RHPF.ar(source[1],LFNoise2.ar(5).range(500,7000),0.4), 
			RLPF.ar(source[1],LFNoise2.ar(5).range(50,2000),0.25), SinOsc.ar(mousey), level)
	];
	source = source + Mix.fill(16,{
		AllpassC.ar(source,0.1,LFTri.kr(rand(0.9)).range(0.001,0.001),0.04)
	});
	source;
}.play
)