
* 0xA chips
** chips/chipno
*** about
    chipno is mostly trial and error experimentation on using microcontroller for making music. It tries to use the wave
    form generators that comes with the MC, as well as using MC to control other sound making circuits. 
*** hardware used
    Atmel Attiny2313
    lm386 (for the pwm amplification)
    sound making circuits from toys
*** notes
    so, attiny2313 has 20 pins, two of which can be used for the outputs for the on-board wave generators. the rest of
    the pins can then be used to interface/control circuits that has been modified to work with it. This can be done by
    using transistor(BJT) to allow the output of MC as on/off switches for whatever that is on the original board. or,
    find a "sweet spot" on the circuit, and then plug the output from the MC straight onto it to "modulate" the changes
    in the sound. anyways, the idea is that a lot can be done with 18 pins;) 
    Also, there are a lot of "music IC" that can be found, like the one in the cheesy music birthday cards... those can
    be fun to add to this setup too, i guess.
*** TODO
    not sure how the resulting sound from all the boards are going to connect to the sound system yet, if there is
    one. so far, i have only been using small speakers(up to 1W)
